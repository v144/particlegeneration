import os

SHOW_STAGING_HOLES = False
SHOW_STAGING_BULGES = False
SHOW_STAGING_ROTATION = False
SHOW_MAIN_ELLIPSOID = False
ROTATE_IN_HOLES_GENERATION = False
MEDIAN_FILTER_SIZE = 15
IMG_SIZE = 100
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

POSTGRES_ADDRESS = os.environ.get('POSTGRES_ADDRESS', 'postgres://particles:postgres@127.0.0.1:5432/particles')
PARTICLE_STORAGE = os.path.join(BASE_DIR, os.environ.get('PARTICLE_STORAGE', './particle_file_store'))
DATA_RANGE_STORAGE = os.path.join(BASE_DIR, os.environ.get('DATA_RANGE_STORAGE', './data_range_store'))
FORM_TYPE = os.environ.get('FORM_TYPE', 'rob')
FORM_TYPE_REPORT_NAME = 'для частиц в форме куба с пористотью 15%'
# FORM_TYPE_REPORT_NAME = 'для частиц в форме стержня без пор'
