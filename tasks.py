# -*- coding: utf-8 -*-
"""Invoke tasks.py

Details see manual: http://docs.pyinvoke.org/en/1.0/getting-started.html
"""
# ThirdParty Library
from invoke import task


@task
def install(ctx):
    """Dependency install."""
    ctx.run('pipenv install')


@task
def yapf(ctx):
    """Run yapf."""
    ctx.run('yapf -i --recursive ./scripts ./common *.py')


@task
def flake8(ctx):
    """Run flake8."""
    ctx.run('flake8 ./scripts ./common *.py')


@task
def isort(ctx):
    """Run isort."""
    ctx.run('isort -rc --atomic ./scripts ./common *.py')


@task(pre=[isort, yapf, flake8])
def lint(ctx):
    """Run lints."""
    pass
