import random

import numpy as np
from numpy import ndarray
from scipy import ndimage

from common.bulges_utils import generate_bulge, get_bulges_point, get_bulges_radius
from common.database_utils import save_particle_porosity
from common.ellipsoid_utils import generate_border_ellipsoid, generate_ellipsoid, random_rotation
from common.form_type import FormType
from common.geometry_utils import (distance_between_two_points, get_min_distance_between_point_and_object,
                                   get_min_distance_to_border_image)
from common.holes_utils import generate_holes, get_holes_point, get_holes_radius
from common.moments_utils import calc_features
from common.save_utlis import save_to_mat, show_img
from settings import FORM_TYPE, IMG_SIZE, MEDIAN_FILTER_SIZE, ROTATE_IN_HOLES_GENERATION, SHOW_MAIN_ELLIPSOID


def generate_radius(imsz):
    r_x = random.randint(imsz // 3, imsz // 2.5)
    # r_y = random.randint(imsz // 5, imsz // 3)
    # r_z = random.randint(imsz // 5, imsz // 3)
    r_y = random.randint(r_x // 5, r_x // 3)
    r_z = random.randint(r_x // 5, r_x // 3)
    axes = [r_x, r_y, r_z]
    random.shuffle(axes)
    return axes


def create_particle(imsz: int, holes: int, bulges: int, entire_holes: int):
    z, y, x = np.indices((imsz + 10, imsz + 10, imsz + 10))
    center = imsz // 2
    rel_const = 0.99
    # r_x, r_y, r_z = generate_radius(imsz)
    r_x, r_y, r_z = FormType().get_sizes(imsz, FORM_TYPE)
    test_image = generate_ellipsoid(x, y, z, center, center, center, r_x, r_y, r_z)
    border_ellipsoid, inner_ellipsoid = generate_border_ellipsoid(x, y, z, center, center, center, r_x * rel_const,
                                                                  r_y * rel_const, r_z * rel_const)
    zp, yp, xp = test_image.nonzero()

    # if SHOW_MAIN_ELLIPSOID:
    #     show_img(test_image, f'ORIGINAL: center: {center}; r: {r_x}, {r_y}, {r_z}')

    # bulge generation
    img_border = border_ellipsoid
    img_inner = inner_ellipsoid
    for _ in range(0, bulges):
        bulge_center_x = get_bulges_point(imsz, center, r_x * rel_const)
        bulge_center_y = get_bulges_point(imsz, center, r_y * rel_const)
        bulge_center_z = get_bulges_point(imsz, center, r_z * rel_const)
        b_zp, b_yp, b_xp = inner_ellipsoid.nonzero()
        min_radius = get_min_distance_between_point_and_object(imsz=imsz,
                                                               xp=b_xp,
                                                               yp=b_yp,
                                                               zp=b_zp,
                                                               center_x=bulge_center_x,
                                                               center_y=bulge_center_y,
                                                               center_z=bulge_center_z)
        min_radius = int(round(min_radius))

        min_distance_to_border = get_min_distance_to_border_image(center_x=bulge_center_x,
                                                                  center_y=bulge_center_y,
                                                                  center_z=bulge_center_z,
                                                                  imsz=imsz)
        min_distance_to_border = int(round(min_distance_to_border))

        distance_between_bulge_and_center = round(
            distance_between_two_points(bulge_center_x, bulge_center_y, bulge_center_z, center, center, center))

        try:
            bulge_r = get_bulges_radius(length=distance_between_bulge_and_center,
                                        min_radius=min_radius,
                                        min_distance_to_border=min_distance_to_border)
        except:  # noqa
            print(f'Incorrect point: {bulge_center_x} {bulge_center_y} {bulge_center_z}')
            continue

        bulges_img = generate_ellipsoid(x, y, z, bulge_center_x, bulge_center_y, bulge_center_z, bulge_r, bulge_r,
                                        bulge_r)
        bulge_border, bulge_inner = generate_border_ellipsoid(x, y, z, bulge_center_x, bulge_center_y, bulge_center_z,
                                                              bulge_r * rel_const, bulge_r * rel_const,
                                                              bulge_r * rel_const)
        comment = f'BULGE: min_rad: {min_radius}, centers - {bulge_center_x} {bulge_center_y} {bulge_center_z};' \
            f' r: {bulge_r}, {bulge_r}, {bulge_r}'
        test_image, img_border, img_inner = generate_bulge(test_image, bulges_img, img_border, img_inner, bulge_border,
                                                           bulge_inner)
        # save_to_mat(test_image)
        print(f' {comment}, next bulge')

    # holes generation
    for _ in range(0, holes):
        hole_center_x = get_holes_point(imsz, center, r_x)
        hole_center_y = get_holes_point(imsz, center, r_y)
        hole_center_z = get_holes_point(imsz, center, r_z)

        min_radius = get_min_distance_between_point_and_object(imsz=imsz,
                                                               xp=xp,
                                                               yp=yp,
                                                               zp=zp,
                                                               center_x=hole_center_x,
                                                               center_y=hole_center_y,
                                                               center_z=hole_center_z)
        min_radius = int(round(min_radius))

        min_distance_to_border = get_min_distance_to_border_image(center_x=hole_center_x,
                                                                  center_y=hole_center_y,
                                                                  center_z=hole_center_z,
                                                                  imsz=imsz)
        min_distance_to_border = int(round(min_distance_to_border))

        distance_between_hole_and_center = round(
            distance_between_two_points(hole_center_x, hole_center_y, hole_center_z, center, center, center))
        try:
            hole_r_x = get_holes_radius(length=distance_between_hole_and_center,
                                        min_radius=min_radius,
                                        min_distance_to_border=min_distance_to_border)
            hole_r_y = get_holes_radius(length=distance_between_hole_and_center,
                                        min_radius=min_radius,
                                        min_distance_to_border=min_distance_to_border)
            hole_r_z = get_holes_radius(length=distance_between_hole_and_center,
                                        min_radius=min_radius,
                                        min_distance_to_border=min_distance_to_border)
        except:  # noqa
            print(f'Incorrect point: {hole_center_x} {hole_center_y} {hole_center_z}')
            continue

        holes_img = generate_ellipsoid(x, y, z, hole_center_x, hole_center_y, hole_center_z, hole_r_x, hole_r_y,
                                       hole_r_z)
        comment = f'HOLES: min_rad: {min_radius}, centers - {hole_center_x},{hole_center_y},{hole_center_z};' \
            f' r: {hole_r_x}, {hole_r_y}, {hole_r_z}'
        if ROTATE_IN_HOLES_GENERATION:
            random_rotation(holes_img, comment)
            comment += ' ROTATION: ON'
        test_image, _ = generate_holes(test_image, holes_img, comment)
        # save_to_mat(test_image)
        print(f' {comment}, next hole')

    # entire holes generation
    for _ in range(0, entire_holes):
        hole_center_x = random.randint(center - r_x // 2, center + r_x // 2)
        hole_center_y = random.randint(center - r_y // 2, center + r_y // 2)
        hole_center_z = random.randint(center - r_z // 2, center + r_z // 2)

        hole_r_x = random.randint(r_x // 6, r_x // 3)
        hole_r_y = random.randint(r_y // 6, r_y // 3)
        hole_r_z = random.randint(r_z // 6, r_z // 3)

        holes_img = generate_ellipsoid(x, y, z, hole_center_x, hole_center_y, hole_center_z, hole_r_x, hole_r_y,
                                       hole_r_z)
        comment = f'HOLES: centers - {hole_center_x},{hole_center_y},{hole_center_z};' \
                  f' r: {hole_r_x}, {hole_r_y}, {hole_r_z}'
        test_image, _ = generate_holes(test_image, holes_img, comment)
        # save_to_mat(test_image)
        print(f' {comment}, next hole')

    # apply filtration
    # show_img(test_image, comment='Before median')
    # save_to_mat(test_image)
    median_size = MEDIAN_FILTER_SIZE
    before_objects, before_amount_of_objects = ndimage.measurements.label(test_image)
    print(f'Before: {before_amount_of_objects}')
    if before_amount_of_objects > 1:
        pass
    # save_to_mat(test_image)
    result = ndimage.median_filter(test_image, median_size)
    after_objects, after_amount_of_objects = ndimage.measurements.label(result)
    print(f'After: {after_amount_of_objects}')
    if after_amount_of_objects == 0:
        print('aaaaa')
    if after_amount_of_objects > 1:
        print('Leave max standalone object')
        result = (after_objects == (np.bincount(after_objects.flat)[1:].argmax() + 1))
        result = result.astype(np.uint8)
    # save_to_mat(result)
    # show_img(result, comment=f'Median {median_size}')
    from common.porosity_utils import generate_internal_holes
    before_porosity = result.copy()
    is_valid, new_porosity_image, porosity = generate_internal_holes(result,
                                                                     center,
                                                                     r_x,
                                                                     r_y,
                                                                     r_z,
                                                                     x,
                                                                     y,
                                                                     z,
                                                                     img_border,
                                                                     img_inner,
                                                                     porosity=0.15)
    if not is_valid:
        return False, None, None, None
    return True, new_porosity_image, porosity, before_porosity


def paralized_function(sizes, offset=0):
    from common.database_utils import save_particle, get_db_connection, save_without_porosity_particle
    connection = get_db_connection()
    with connection:
        for distribution_index, size in enumerate(sizes):
            # hole_amount = random.randint(1, 3)
            hole_amount = 0  # now it is in porosity
            bulge_amount = random.randint(4, 10)
            # entire_holes_amount = random.randint(1, 5)
            entire_holes_amount = 0
            while True:
                is_valid, particle_image, porosity, before_porosity = create_particle(
                    size, hole_amount, bulge_amount, entire_holes_amount)
                if is_valid:
                    break

            img_uuid = save_particle(connection, int(size), hole_amount, bulge_amount, distribution_index + offset,
                                     particle_image, entire_holes_amount)
            save_without_porosity_particle(img_uuid, int(size), before_porosity)
            save_particle_porosity(connection, img_uuid, {'porosity': porosity})
            print(f'{img_uuid} - {distribution_index}')


def paralized_function_to_continue(sizes_with_index):
    import multiprocessing
    from common.database_utils import save_particle, get_db_connection, save_without_porosity_particle
    connection = get_db_connection()
    with connection:
        try:
            for size, distribution_index in sizes_with_index:
                # hole_amount = random.randint(1, 3)
                hole_amount = 0  # now it is in porosity
                bulge_amount = random.randint(1, 5)
                # entire_holes_amount = random.randint(1, 5)
                entire_holes_amount = 0
                while True:
                    is_valid, particle_image, porosity, before_porosity = create_particle(
                        size, hole_amount, bulge_amount, entire_holes_amount)
                    if is_valid:
                        break
                    print('generate next particle instead fail - ', multiprocessing.current_process().name)

                img_uuid = save_particle(connection, int(size), hole_amount, bulge_amount, int(distribution_index),
                                         particle_image, entire_holes_amount)
                save_without_porosity_particle(img_uuid, int(size), before_porosity)
                save_particle_porosity(connection, img_uuid, {'porosity': porosity})
                print(f'{img_uuid} - {distribution_index}')

        except Exception as err:
            print('ALARMAAAA')
            import traceback
            traceback.print_tb(err.__traceback__)


def generate_particles_database_paralized():
    from common.norm_particle_generator import get_normal_dist, show_normal_dist, save_dist
    import os
    import multiprocessing
    sizes = get_normal_dist(10, 50, 1000, 150)
    show_normal_dist(sizes, 50)
    save_dist(sizes)

    cpus = os.cpu_count()
    samples = np.array_split(sizes, cpus)
    pool = multiprocessing.Pool()  # use all available cores, otherwise specify the number you want as an argument
    offset = 0
    for sample in samples:
        if len(sample) == 0:
            continue
        print('run next')
        pool.apply_async(paralized_function, args=(sample, offset))
        offset += len(sample)
    pool.close()
    pool.join()


def continue_generate_particles_from_samples(samples_indexes):
    import os
    import multiprocessing
    cpus = os.cpu_count()
    samples_with_indexes = np.array_split(samples_indexes, cpus)
    pool = multiprocessing.Pool(cpus)  # use all available cores, otherwise specify the number you want as an argument
    for sample_with_index in samples_with_indexes:
        if len(sample_with_index) == 0:
            continue
        # paralized_function_to_continue(sample_with_index)
        print('run next')
        pool.apply_async(paralized_function_to_continue, args=(sample_with_index,))
    pool.close()
    pool.join()


def generate_particles_database():
    from common.norm_particle_generator import get_normal_dist, show_normal_dist, save_dist
    sizes = get_normal_dist(10, 50, 1000, 150)
    show_normal_dist(sizes, 50)
    # save_dist(sizes)
    paralized_function(sizes, 0)


if __name__ == '__main__':
    generate_particles_database()
    # generate_particles_database_paralized()
    from common.process_particles.calc_features import calc_all_images_features_paralyzed, get_indexes
    # #
    # continue_generate_particles_from_samples(get_indexes())
    # #
    # calc_all_images_features_paralyzed()
    # image = create_particle(200, 0, 3, 0)
    # import scipy.io as scio
    #
    # scio.savemat('test_image.mat', dict(img=image))
    # from common.process_particles import calc_features_for_image_group
    # img_uuid = '7e1191e8-55d4-4c0a-8fec-9a453dc4ed0d'
    # img_size = 118
    # calc_features_for_image_group([(img_uuid, img_size)])
