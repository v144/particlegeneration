import os
from collections import defaultdict

import cv2
import numpy as np
import openpyxl.utils
import pandas as pd
import scipy.io as scio
import scipy.stats as scistats
import json

import settings

def plot_img_and_hist(datas, bins, names, title=None, xlabel=None, ylabel=None):
    """Plot an image along with its histogram and cumulative histogram.

    """
    import matplotlib
    import matplotlib.pyplot as plt
    import numpy as np

    from skimage import data, img_as_float
    from skimage import exposure

    matplotlib.rcParams['font.size'] = 24
    import seaborn as sns
    markers = ["o", "<", ">", "P", "+", "x", "D", "*"]
    sns.set_style("darkgrid")
    used_markers = []
    legend = {}
    fig, ax = plt.subplots(figsize=(21, 12))
    ghgh = ax
    for i, data in enumerate(datas):
        img_cdf, bin_edges = exposure.cumulative_distribution(data, bins)

        ghgh.plot((bin_edges[:] - min(bin_edges)) / (max(bin_edges) - min(bin_edges)), img_cdf/ img_cdf[-1],
                 marker=markers[i], label=names[i])
        used_markers.append(markers[i])
        legend[names[i]] = markers[i]
    ghgh.legend(handler_map=legend)
    if title:
        plt.title(title)
    if xlabel:
        plt.xlabel(xlabel)
    if ylabel:
        plt.ylabel(ylabel)
    plt.savefig(f"{title}.svg")
    plt.show()

NAMING = {
    'shaking': 'эталон',
    'shaking_without_holes': 'эталон без дыр',
    'min_shaking': 'эталон',
    'min_shaking_without_holes': 'эталон без дыр',
    'ed': 'эквивалентый диаметр',
    'ed_without_holes': 'эквивалентый диаметр без дыр',
    'max_feret': 'макс диаметр Фере',
    'min_feret': 'мин диаметр Фере',
    'mean_feret': 'ср. диаметр Фере',
    'min_semi_axe': 'мин из полуосей (a,b,c) \n (при помощи тензора инерции)',
    'max_semi_axe': 'макс из полуосей (a,b,c) \n (при помощи тензора инерции)',
    'mean_semi_axe': 'ср из полуосей (a,b,c) \n (при помощи тензора инерции)',
    'max_tensor_axe': 'макс длина проекций на главную ось \n (при помощи тензора инерции)',
    'mean_tensor_axe': 'ср длина проекций на главную ось \n (при помощи тензора инерции)',
    'min_tensor_axe': 'мин длина проекций на главную ось \n (при помощи тензора инерции)',
    'min_semi_axe_without_holes': 'мин из полуосей (a,b,c) \n (при помощи тензора инерции) без дыр',
    'max_semi_axe_without_holes': 'макс из полуосей (a,b,c) \n (при помощи тензора инерции) без дыр',
    'mean_semi_axe_without_holes': 'ср из полуосей (a,b,c) \n (при помощи тензора инерции) без дыр',
    'max_tensor_axe_without_holes': 'макс длина проекций на главную ось \n (при помощи тензора инерции) без дыр',
    'mean_tensor_axe_without_holes': 'ср длина проекций на главную ось \n (при помощи тензора инерции) без дыр',
    'min_tensor_axe_without_holes': 'мин длина проекций на главную ось \n (при помощи тензора инерции) без дыр',
    'HISTCMP_INTERSECT': 'Intersection',
    'HISTCMP_KL_DIV': 'Расстояние Кульбака — Лейблера',
    'HISTCMP_CHISQR_ALT': 'Расстояние хи-квадрат альтернативное',
    'HISTCMP_BHATTACHARYYA': 'Расстояние Бхатачария',
    'HISTCMP_CHISQR': 'Расстояние хи-квадрат',
    'HISTCMP_HELLINGER': 'Расстояние Хеллингера',
    'HISTCMP_CORREL': 'Kоэффициент корреляции',
    'median_semi_axe': 'медиана полуосей (a,b,c)',
    'median_tensor_axe': 'медиана длина проекций на главную ось',
    'mean_from2smallest_semi_axe': 'среднее из двух наим длин полуосей',
    'mean_from2smallest_tensor_axe': 'среднее из двух наим длин проекций на главную ось',
    'std_from2smallest_semi_axe': 'средне квадратичное из двух наим длин полуосей',
    'std_from2smallest_tensor_axe': 'средне квадратичное из двух наим длин проекций на главную ось',
    'gmean_from2smallest_semi_axe': 'средне геометрическое из двух наим длин полуосей',
    'gmean_from2smallest_tensor_axe': 'средне геометрическое из двух наим длин проекций на главную ось',
    'median_semi_axe_without_holes': 'медиана полуосей (a,b,c) без дыр',
    'median_tensor_axe_without_holes': 'медиана длина проекций на главную ось без дыр',
    'mean_from2smallest_semi_axe_without_holes': 'среднее из двух наим длин полуосей без дыр',
    'mean_from2smallest_tensor_axe_without_holes': 'среднее из двух наим длин проекций на главную ось без дыр',
    'std_from2smallest_semi_axe_without_holes': 'средне квадратичное из двух наим длин полуосей без дыр',
    'std_from2smallest_tensor_axe_without_holes':
    'средне квадратичное из двух наим длин проекций на главную ось без дыр',
    'gmean_from2smallest_semi_axe_without_holes': 'средне геометрическое из двух наим длин полуосей без дыр',
    'gmean_from2smallest_tensor_axe_without_holes':
    'средне геометрическое из двух наим длин проекций на главную ось без дыр',
    'gmean_feret_ap': 'средне геометрическое по тройкам диамеров Фере',
    'std_feret_ap': 'средне квадратичное по тройкам диамеров Фере',
    'mean_feret_ap': 'среднее по тройкам диамеров Фере',
    'median_feret': 'медиана диаметров Фере',
    'max_feret_est': 'макс оценка диаметров Фере',
    'min_feret_est': 'мин оценка диаметров Фере',
    'mean_feret_est': 'ср оценка диаметров Фере',
}

EXCEL_NAMING = {
    'HISTCMP_INTERSECT': 'Dis',
    'HISTCMP_KL_DIV': 'Dkl',
    'HISTCMP_BHATTACHARYYA': 'Dbh',
    'HISTCMP_CHISQR': 'Dchi',
    'HISTCMP_CORREL': 'Dcor',
    'shaking': 'эталон',
    'shaking_without_holes': 'эталон без дыр',
    'min_shaking': 'эталон',
    'min_shaking_without_holes': 'эталон без дыр',
    'ed': 'ED',
    'ed_without_holes': 'ED_WH',
    'max_feret': 'MAX_FERET',
    'min_feret': 'MIN_FERET',
    'mean_feret': 'MEAN_FERET',
    'min_semi_axe': 'MIN_SA',
    'max_semi_axe': 'MAX_SA',
    'mean_semi_axe': 'MEAN_SA',
    'max_tensor_axe': 'MAX_SR',
    'mean_tensor_axe': 'MEAN_SR',
    'min_tensor_axe': 'MIN_SR',
    'min_semi_axe_without_holes': 'MIN_SA_WH',
    'max_semi_axe_without_holes': 'MAX_SA_WH',
    'mean_semi_axe_without_holes': 'MEAN_SA_WH',
    'max_tensor_axe_without_holes': 'MAX_SR_WH',
    'mean_tensor_axe_without_holes': 'MEAN_SR_WH',
    'min_tensor_axe_without_holes': 'MIN_SR_WH',
    'median_semi_axe': 'MEDIAN_SA',
    'median_tensor_axe': 'MEDIAN_SR',
    'mean_from2smallest_semi_axe': 'MEAN_SA_MIN_MEDIAN',
    'mean_from2smallest_tensor_axe': 'MEAN_SR_MIN_MEDIAN',
    'std_from2smallest_semi_axe': 'MSQUARE_SA_MIN_MEDIAN',
    'std_from2smallest_tensor_axe': 'MSQUARE_SR_MIN_MEDIAN',
    'gmean_from2smallest_semi_axe': 'GMEAN_SA_MIN_MEDIAN',
    'gmean_from2smallest_tensor_axe': 'GMEAN_SR_MIN_MEDIAN',
    'median_semi_axe_without_holes': 'MEDIAN_SA_WH',
    'median_tensor_axe_without_holes': 'MEDIAN_SR_WH',
    'mean_from2smallest_semi_axe_without_holes': 'MEAN_SA_MIN_MEDIAN_WH',
    'mean_from2smallest_tensor_axe_without_holes': 'MEAN_SR_MIN_MEDIAN_WH',
    'std_from2smallest_semi_axe_without_holes': 'MSQUARE_SA_MIN_MEDIAN_WH',
    'std_from2smallest_tensor_axe_without_holes':
        'MSQUARE_SR_MIN_MEDIAN_WH',
    'gmean_from2smallest_semi_axe_without_holes': 'GMEAN_SA_MIN_MEDIAN',
    'gmean_from2smallest_tensor_axe_without_holes':
        'GMEAN_SR_MIN_MEDIAN',
    'gmean_feret_ap': 'GMEAN_FERET_3',
    'std_feret_ap': 'MSQUARE_FERET_3',
    'mean_feret_ap': 'MEAN_FERET_3',
    'median_feret': 'MEDIAN_FERET',
    'max_feret_est': 'MAX_E1',
    'min_feret_est': 'MIN_E1',
    'mean_feret_est': 'MEAN_E1',
    'shaking': 'эталон'
}


def create_excel_with_dict(data: dict, row_amount: int, filename, width=50, height=None):
    table = pd.DataFrame(data)
    writer = pd.ExcelWriter(f'{filename}.xlsx')
    table.to_excel(writer, 'Results')

    for i in range(1, row_amount + 1):
        writer.sheets['Results'].column_dimensions[openpyxl.utils.cell.get_column_letter(i)].width = width
        if height is not None:
            writer.sheets['Results'].row_dimensions[i+1].height = height
    writer.save()


def round2(x, to):
    return {'statistic': round(x[0], to), 'pvalue': round(x[1], to)}

def get_data_(filename):
    img = scio.loadmat(os.path.join(settings.DATA_RANGE_STORAGE, f'{filename}.mat'))['r']
    data = img[0]
    return data

def get_data_range(filename, bins, normed=False):
    img = scio.loadmat(os.path.join(settings.DATA_RANGE_STORAGE, f'{filename}.mat'))['r']
    data = img[0]
    hist_data = np.histogram(data, bins=bins, normed=normed)
    # because of opencv
    return hist_data, hist_data[0].astype(dtype="float32")


def compare_hist(raw1, raw2):
    return {
        'HISTCMP_INTERSECT': cv2.compareHist(raw1, raw2, cv2.HISTCMP_INTERSECT),
        'HISTCMP_KL_DIV': cv2.compareHist(raw1, raw2, cv2.HISTCMP_KL_DIV),
        'HISTCMP_CHISQR_ALT': cv2.compareHist(raw1, raw2, cv2.HISTCMP_CHISQR_ALT),
        'HISTCMP_BHATTACHARYYA': cv2.compareHist(raw1, raw2, cv2.HISTCMP_BHATTACHARYYA),
        'HISTCMP_CORREL': cv2.compareHist(raw1, raw2, cv2.HISTCMP_CORREL),
        'HISTCMP_CHISQR': cv2.compareHist(raw1, raw2, cv2.HISTCMP_CHISQR),
        # 'HISTCMP_HELLINGER': cv2.compareHist(raw1, raw2, cv2.HISTCMP_HELLINGER), because it is equal to HISTCMP_BHATTACHARYYA
    }


def prepare_and_comparing():
    compared = 'shaking'
    pairs = (
        # always
        (compared, 'min_feret'),
        (compared, 'mean_feret'),
        (compared, 'gmean_feret_ap'),
        (compared, 'std_feret_ap'),
        (compared, 'mean_feret_ap'),
        (compared, 'median_feret'),
        (compared, 'max_feret_est'),
        (compared, 'min_feret_est'),
        (compared, 'mean_feret_est'),

        # with pores
        (compared, 'ed'),
        (compared, 'min_semi_axe'),
        (compared, 'mean_semi_axe'),
        (compared, 'min_tensor_axe'),
        (compared, 'mean_tensor_axe'),
        (compared, 'median_semi_axe'),
        (compared, 'median_tensor_axe'),
        (compared, 'mean_from2smallest_semi_axe'),
        (compared, 'mean_from2smallest_tensor_axe'),
        (compared, 'std_from2smallest_semi_axe'),
        (compared, 'std_from2smallest_tensor_axe'),
        (compared, 'gmean_from2smallest_semi_axe'),
        (compared, 'gmean_from2smallest_tensor_axe'),

        # # without pores
        # (compared, 'ed_without_holes'),
        # (compared, 'min_semi_axe_without_holes'),
        # (compared, 'mean_semi_axe_without_holes'),
        # (compared, 'min_tensor_axe_without_holes'),
        # (compared, 'mean_tensor_axe_without_holes'),
        #
        # (compared, 'median_semi_axe_without_holes'),
        # (compared, 'median_tensor_axe_without_holes'),
        # (compared, 'mean_from2smallest_semi_axe_without_holes'),
        # (compared, 'mean_from2smallest_tensor_axe_without_holes'),
        # (compared, 'std_from2smallest_semi_axe_without_holes'),
        # (compared, 'std_from2smallest_tensor_axe_without_holes'),
        # (compared, 'gmean_from2smallest_semi_axe_without_holes'),
        # (compared, 'gmean_from2smallest_tensor_axe_without_holes'),


    )
    # set auto or calc manual from shaking
    bins = 'auto'
    hist_data_1, data_range1 = get_data_range(compared, bins=bins)
    prepared_data = {'shaking': (hist_data_1, data_range1)}
    print(compared, scistats.kstest(data_range1, 'norm'))
    result = defaultdict(list)
    new_result = defaultdict(lambda: defaultdict(dict))
    hypothesis_result = defaultdict(lambda: defaultdict(dict))

    refs = {'shaking': get_data_('shaking')}

    for filename1, filename2 in pairs:
        # prepared_data.get(filename1, get_data_range(filename1))
        hist_data_2, data_range2 = get_data_range(filename2, bins=len(data_range1))
        refs[filename2] = get_data_('shaking')
        compare_result = compare_hist(data_range1, data_range2)
        for k, v in compare_result.items():
            result[k].append((v, filename2))
        print(f'{filename1}:{filename2}', compare_hist(data_range1, data_range2))
        print(filename2, scistats.kstest((data_range2 - data_range2.mean()) / data_range2.std(), 'norm'))
        name_comparing = f'{NAMING[filename1]}:{NAMING[filename2]}'
        hypothesis_result['m1 = m2 ttest_ind'][name_comparing] = round2(scistats.ttest_ind(data_range1, data_range2), 3)
        hypothesis_result['m1 = m2 ttest_rel'][name_comparing] = round2(scistats.ttest_rel(data_range1, data_range2), 3)
        hypothesis_result['v1 = v2 fligner'][name_comparing] = round2(scistats.fligner(data_range1, data_range2), 3)
        hypothesis_result['v1 = v2 bartlett'][name_comparing] = round2(scistats.bartlett(data_range1, data_range2), 3)
        # hypothesis_result['v1 = v2 levene'][name_comparing] = scistats.levene(data_range1, data_range2) if it was not normal
        hypothesis_result['median1 = median12 kruskal'][name_comparing] = round2(
            scistats.kruskal(data_range1, data_range2), 3)
        hypothesis_result['is norm kstest'][name_comparing] = round2(
            scistats.kstest((data_range2 - data_range2.mean()) / data_range2.std(), 'norm'), 3)
        hypothesis_result['both samples are drawn from the same distribution - ks_2samp'][name_comparing] = round2(
            scistats.ks_2samp(data_range1, data_range2), 3)
    new_new_result = defaultdict(lambda: defaultdict(dict))
    for k, v in result.items():
        if k == 'HISTCMP_CHISQR_ALT':
            continue
        if k == 'HISTCMP_CORREL' or k == 'HISTCMP_INTERSECT':
            print(k, json.dumps(sorted(v, reverse=True)[:5], indent=1))
        else:
            print(k, json.dumps(sorted(v, reverse=False)[:5], indent=1))
        mp_amount = defaultdict(int)
        for k, v in result.items():
            # new_result[NAMING[diagram_name]][NAMING[k]] = value
            # because we have much more features than algoritms
            if k == 'HISTCMP_CHISQR_ALT':
                continue
            if k == 'HISTCMP_CORREL' or k == 'HISTCMP_INTERSECT':
                for inde, value in enumerate(sorted(v, reverse=True)[:4]):
                    exc_val = round(value[0], 3)
                    # new_new_result[EXCEL_NAMING[k]][inde+1] = f'{exc_val}:{EXCEL_NAMING[value[1]]}'
                    new_new_result[inde + 1][EXCEL_NAMING[k]] = f'{exc_val}\n{EXCEL_NAMING[value[1]]}'
                    mp_amount[value[1]] += 1

            else:
                for inde, value in enumerate(sorted(v, reverse=False)[:4]):
                    exc_val = round(value[0], 3)
                    # new_new_result[EXCEL_NAMING[k]][inde+1] = f'{exc_val}:{EXCEL_NAMING[value[1]]}'
                    new_new_result[inde + 1][EXCEL_NAMING[k]] = f'{exc_val}\n{EXCEL_NAMING[value[1]]}'
                    mp_amount[value[1]] += 1

    sorted_mp_amount = sorted(mp_amount.items(), key=lambda p: p[1], reverse=True)
    print(sorted_mp_amount)
    datas = [refs['shaking']]
    names = [EXCEL_NAMING['shaking']]
    for mp, _ in sorted_mp_amount:
        datas.append(get_data_(mp))
        names.append(EXCEL_NAMING[mp])
    plot_img_and_hist(datas,len(data_range1), names, xlabel='Нормированное значение морфометрического признака',
                      ylabel='Процент, прошедших частиц в ячейку',
                      title=f'Распределения по размеру при гранулометрическом анализе {settings.FORM_TYPE_REPORT_NAME}')
    create_excel_with_dict(new_result, len(pairs), 'HistogramDistances')
    create_excel_with_dict(new_new_result, len(pairs), 'HistogramDistancesNew', height=32, width=30)
    create_excel_with_dict(hypothesis_result, 10, 'hypothesisResults', width=50)


if __name__ == '__main__':
    prepare_and_comparing()


def plotcum1(datas, n_bins, histtype='step'):
    import numpy as np
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    for data in datas:
        n, bins, patches = ax.hist(data, n_bins, density=True, histtype=histtype,
                                   cumulative=True, label='Empirical')
    ax.grid(True)
    ax.legend(loc='right')
    ax.set_title('Cumulative step histograms')
    ax.set_xlabel('Annual rainfall (mm)')
    ax.set_ylabel('Likelihood of occurrence')

    plt.show()

def plotcum2(datas, numbins=25):
    import matplotlib.pyplot as plt
    from scipy import stats
    fig, ax2 = plt.subplots()
    for data in datas:
        res = stats.cumfreq(data, numbins=numbins)
        x = res.lowerlimit + np.linspace(0, res.binsize * res.cumcount.size, res.cumcount.size)
        ax2.bar(x, res.cumcount, width=res.binsize)

    ax2.set_xlim([x.min(), x.max()])

    ax2.set_title('Cumulative histogram')


    plt.show()


def plotcum3(data, num_bins, title=None, xlabel=None):
    import matplotlib.pyplot as plt
    import seaborn as sns
    counts, bin_edges = np.histogram(data, bins=num_bins, normed=True)
    cdf = np.cumsum(counts)

    sns.set_style("darkgrid")
    plt.figure(figsize=(18, 13))
    plt.plot((bin_edges[1:] - min(bin_edges)) / (max(bin_edges) - min(bin_edges)), cdf / cdf[-1])
    plt.ylabel('CDF')

    if title:
        plt.title(title)
    if xlabel:
        plt.xlabel(xlabel)
    plt.show()


