import os

import numpy as np
import scipy.io as scio
from scipy.stats.mstats import gmean

import settings
from common.database_utils import fetchall, fetchall_with_kwargs, get_db_connection
from common.database_utils.feature_queries import (
    GET_ED, GET_GMEAN_AP_FERET, GET_MAX_FERET, GET_MAX_FERET_ESTIMATION, GET_MAX_SEMI_AXE, GET_MAX_SHAKING,
    GET_MAX_TENSOR_AXE, GET_MEAN_AP_FERET, GET_MEAN_FERET, GET_MEAN_FERET_ESTIMATION, GET_MEAN_SEMI_AXE,
    GET_MEAN_TENSOR_AXE, GET_MEDIAN_FERET, GET_MIN_FERET, GET_MIN_FERET_ESTIMATION, GET_MIN_SEMI_AXE, GET_MIN_SHAKING,
    GET_MIN_TENSOR_AXE, GET_SEMI_AXES, GET_STD_AP_FERET, GET_TENSOR_AXES, GET_VALID_SHAKING)

cached_data = {
    GET_SEMI_AXES: None,
    GET_TENSOR_AXES: None,
}


def select_data_and_save(conn, q, filename, data_func, with_holes):
    if cached_data.get(f'{q}_{with_holes}') is None:
        if 'feret' in filename:
            data = fetchall(conn, q)
        else:
            data = fetchall_with_kwargs(conn, q, with_holes)
        cached_data[f'{q}_{with_holes}'] = data
    else:
        data = cached_data.get(f'{q}_{with_holes}')
    data_values = [data_func(i) for i in data]
    scio.savemat(os.path.join(settings.DATA_RANGE_STORAGE, f'{filename}.mat'), dict(r=data_values))


def prepare_and_run_save_data():
    os.makedirs(settings.DATA_RANGE_STORAGE, exist_ok=True)
    conn = get_db_connection()
    pairs = (
        (GET_ED, 'ed_without_holes', lambda x: x[0], False),
        # (GET_MAX_SHAKING, 'shaking_without_holes', lambda x: x[0], False),
        (GET_VALID_SHAKING, 'shaking_without_holes', lambda x: x[0], False),
        (GET_MIN_SHAKING, 'min_shaking_without_holes', lambda x: x[0], False),
        (GET_MIN_SEMI_AXE, 'min_semi_axe_without_holes', lambda x: x[0], False),
        (GET_MAX_SEMI_AXE, 'max_semi_axe_without_holes', lambda x: x[0], False),
        (GET_MEAN_SEMI_AXE, 'mean_semi_axe_without_holes', lambda x: x[0], False),
        (GET_MAX_TENSOR_AXE, 'max_tensor_axe_without_holes', lambda x: x[0], False),
        (GET_MIN_TENSOR_AXE, 'min_tensor_axe_without_holes', lambda x: x[0], False),
        (GET_MEAN_TENSOR_AXE, 'mean_tensor_axe_without_holes', lambda x: x[0], False),
        (GET_SEMI_AXES, 'median_semi_axe_without_holes', lambda x: np.median(x), False),
        (GET_TENSOR_AXES, 'median_tensor_axe_without_holes', lambda x: np.median(x), False),
        (GET_SEMI_AXES, 'mean_from2smallest_semi_axe_without_holes', lambda x: np.mean(np.sort(x)[:-1]), False),
        (GET_TENSOR_AXES, 'mean_from2smallest_tensor_axe_without_holes', lambda x: np.mean(np.sort(x)[:-1]), False),
        (GET_SEMI_AXES, 'std_from2smallest_semi_axe_without_holes',
         lambda x: np.sqrt(np.sum(np.square(np.sort(x)[:-1]))), False),
        (GET_TENSOR_AXES, 'std_from2smallest_tensor_axe_without_holes',
         lambda x: np.sqrt(np.sum(np.square(np.sort(x)[:-1]))), False),
        (GET_SEMI_AXES, 'gmean_from2smallest_semi_axe_without_holes', lambda x: gmean(np.sort(x)[:-1]), False),
        (GET_TENSOR_AXES, 'gmean_from2smallest_tensor_axe_without_holes', lambda x: gmean(np.sort(x)[:-1]), False),
        (GET_ED, 'ed', lambda x: x[0], True),
        # (GET_MAX_SHAKING, 'shaking', lambda x: x[0], True),
        (GET_VALID_SHAKING, 'shaking', lambda x: x[0], True),
        (GET_MIN_SHAKING, 'min_shaking', lambda x: x[0], True),
        (GET_MIN_SEMI_AXE, 'min_semi_axe', lambda x: x[0], True),
        (GET_MAX_SEMI_AXE, 'max_semi_axe', lambda x: x[0], True),
        (GET_MEAN_SEMI_AXE, 'mean_semi_axe', lambda x: x[0], True),
        (GET_MAX_TENSOR_AXE, 'max_tensor_axe', lambda x: x[0], True),
        (GET_MIN_TENSOR_AXE, 'min_tensor_axe', lambda x: x[0], True),
        (GET_MEAN_TENSOR_AXE, 'mean_tensor_axe', lambda x: x[0], True),
        (GET_SEMI_AXES, 'median_semi_axe', lambda x: np.median(x), True),
        (GET_TENSOR_AXES, 'median_tensor_axe', lambda x: np.median(x), True),
        (GET_SEMI_AXES, 'mean_from2smallest_semi_axe', lambda x: np.mean(np.sort(x)[:-1]), True),
        (GET_TENSOR_AXES, 'mean_from2smallest_tensor_axe', lambda x: np.mean(np.sort(x)[:-1]), True),
        (GET_SEMI_AXES, 'std_from2smallest_semi_axe', lambda x: np.sqrt(np.sum(np.square(np.sort(x)[:-1]))), True),
        (GET_TENSOR_AXES, 'std_from2smallest_tensor_axe', lambda x: np.sqrt(np.sum(np.square(np.sort(x)[:-1]))), True),
        (GET_SEMI_AXES, 'gmean_from2smallest_semi_axe', lambda x: gmean(np.sort(x)[:-1]), True),
        (GET_TENSOR_AXES, 'gmean_from2smallest_tensor_axe', lambda x: gmean(np.sort(x)[:-1]), True),
        (GET_MAX_FERET, 'max_feret', lambda x: x[0], False),
        (GET_MIN_FERET, 'min_feret', lambda x: x[0], False),
        (GET_MEAN_FERET, 'mean_feret', lambda x: x[0], False),
        (GET_GMEAN_AP_FERET, 'gmean_feret_ap', lambda x: x[0], False),
        (GET_STD_AP_FERET, 'std_feret_ap', lambda x: x[0], False),
        (GET_MEAN_AP_FERET, 'mean_feret_ap', lambda x: x[0], False),
        (GET_MEDIAN_FERET, 'median_feret', lambda x: x[0], False),
        (GET_MAX_FERET_ESTIMATION, 'max_feret_est', lambda x: x[0], False),
        (GET_MEAN_FERET_ESTIMATION, 'mean_feret_est', lambda x: x[0], False),
        (GET_MIN_FERET_ESTIMATION, 'min_feret_est', lambda x: x[0], False),
    )
    with conn:
        for q, filename, data_func, with_holes in pairs:
            select_data_and_save(conn, q, filename, data_func, with_holes)


if __name__ == '__main__':
    prepare_and_run_save_data()
