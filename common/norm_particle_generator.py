import os

import matplotlib.pyplot as plt
import scipy.io as scio
from scipy.stats import truncnorm

from settings import PARTICLE_STORAGE


def save_dist(X):
    os.makedirs(PARTICLE_STORAGE, exist_ok=True)
    scio.savemat(os.path.join(PARTICLE_STORAGE, 'dist.mat'), dict(X=X))


def show_normal_dist(X, values_range):
    bins = 2 * values_range + 1
    plt.close()
    plt.hist(X, bins)
    plt.show()


def get_normal_dist(scale, values_range, size, point=0):
    X = truncnorm(a=-values_range / scale, b=+values_range / scale, scale=scale, loc=point).rvs(size=size)
    X = X.round().astype(int)
    return X
