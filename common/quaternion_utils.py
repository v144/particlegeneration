from itertools import combinations, combinations_with_replacement

import numpy as np
import transformations as tf
from scipy.spatial.transform import Rotation, Slerp


def xyzw_to_wxyz(q):
    return np.array([q[3], q[0], q[1], q[2]])


def wxyz_to_xyzw(q):
    return np.array([q[1], q[2], q[3], q[0]])


def get_rotation_matrixs_by_quats(quat0, quat1, rotations):
    key_rots = Rotation.from_quat([quat0, quat1])
    slerp = Slerp([0, rotations - 1], key_rots)
    times = [i for i in range(0, rotations - 1)]
    # origin, xaxis, yaxis, zaxis = (0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1)
    for fraction in times:
        # alpha, beta, gamma = slerp([fraction]).as_euler('xyz')[0]
        # qx = tf.quaternion_about_axis(alpha, xaxis)
        # qy = tf.quaternion_about_axis(beta, yaxis)
        # qz = tf.quaternion_about_axis(gamma, zaxis)
        # q = tf.quaternion_multiply(qx, qy)
        # q = tf.quaternion_multiply(q, qz)
        # # todo: maybe change order?
        # Rq = tf.quaternion_matrix(q)
        # TODO: think about it
        final_quat = slerp([fraction])._quat[0]
        # Rq = tf.rotation_from_matrix(xyzw_to_wxyz(final_quat))
        Rq = tf.quaternion_matrix(xyzw_to_wxyz(final_quat))
        yield Rq[:3, :3]


def get_all_rotation_matrixs(rotations):
    quat0 = np.array([0.0, 0.0, 0.0, 1.0])  # x,y,z,w notation
    quats = combinations_with_replacement([0, 1, -1], 3)
    for quat in quats:
        if quat == (0, 0, 0):
            continue
        real_quat = quat + (0,)
        for rotation_matrix in get_rotation_matrixs_by_quats(quat0, real_quat, rotations):
            yield rotation_matrix
