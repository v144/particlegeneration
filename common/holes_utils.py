import random
from typing import Optional

from numpy import ndarray

from settings import SHOW_STAGING_HOLES

from .save_utlis import show_img


def generate_holes(img: ndarray, bulge: ndarray, comment: Optional[str] = None, internal_ellipsoid=None):
    z, y, x = bulge.nonzero()
    result_inner = None
    if internal_ellipsoid is not None:
        internal_ellipsoid_copy = internal_ellipsoid.copy()
        internal_ellipsoid_copy[z, y, x] = 0
        result_inner = internal_ellipsoid_copy
    img[z, y, x] = 0
    if SHOW_STAGING_HOLES:
        show_img(img, comment)
    return img, result_inner


def get_holes_point(imsz: int, center: int, r: int) -> int:
    left_bound = (center - r) // 2
    return random.randint(left_bound + 1, imsz - left_bound - 1)


def get_holes_radius(length: int, min_radius: int, min_distance_to_border: int):
    max_radius = min(min_distance_to_border, length)
    if min_radius > max_radius:
        raise Exception("")
    return random.randint(min_radius, max_radius)
