import random
from typing import Optional

import numpy as np
from numpy import ndarray

from settings import SHOW_STAGING_BULGES

from .save_utlis import show_img


def generate_bulge(
        img: ndarray,
        bulge: ndarray,
        img_border,
        img_inner,
        bulge_border,
        bulge_inner,
        comment: Optional[str] = None,
):
    result_inner = img_inner.copy() + bulge_inner.copy()
    result_inner[np.where(result_inner == 2)] = 1
    img_inner[np.where(img_inner == 1)] = 3
    bulge_inner[np.where(bulge_inner == 1)] = 3
    result_border = img_border + img_inner + bulge_border + bulge_inner
    result_border[np.where(result_border == 2)] = 1
    result_border[np.where(result_border == 3)] = 0
    result_border[np.where(result_border == 4)] = 0
    result_border[np.where(result_border == 6)] = 0

    result = img + bulge
    result[np.where(result == 2)] = 1
    if SHOW_STAGING_BULGES:
        show_img(result, comment)
    return result, result_border, result_inner


def get_bulges_point(imsz, center, r):
    r = int(r)
    return random.randint(center - r, center + r)


def get_bulges_radius(length, min_radius, min_distance_to_border):
    max_radius = min(min_distance_to_border, length) // 2
    if min_radius > max_radius or max_radius == 0:
        raise Exception("")
    # to avoid division by zero
    if min_radius == 0:
        min_radius = 1
    return random.randint(min_radius, max_radius)
