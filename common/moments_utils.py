import random

import numpy as np
from numpy import cross, eye
from numpy import linalg as LA
from scipy.linalg import expm, norm
from skimage import measure


def calc_relation_closed_holes(im3d):
    from scipy import ndimage
    from skimage import measure
    props = measure.regionprops(im3d)
    image = np.uint8(props[0].image)
    new_img = ndimage.morphology.binary_fill_holes(image.copy()).astype(int)
    props_new = measure.regionprops(new_img)
    return {'porosity': (props_new[0].area - props[0].area) / props[0].area}


def get_rotation_matrix(axis, theta):
    return expm(cross(eye(3), axis / norm(axis) * theta))


def shaking_emulation(image):
    props = measure.regionprops(image)
    coords = props[0].coords
    axis = [4, 4, 1]
    # TODO: maybe generate 1000 angles evenly increasing between 0 and 2 * np.pi and random choice from it?
    result = []
    # plt.imshow(image.any(axis=2) * 255); plt.show()
    for i in range(0, 1000):
        theta = round(random.uniform(0, 2 * np.pi), 5)
        rotation_matrix = get_rotation_matrix(axis, theta)
        new_coords = np.matmul(coords, rotation_matrix)
        l1 = np.max(new_coords[:, 0]) - np.min(new_coords[:, 0])
        l2 = np.max(new_coords[:, 1]) - np.min(new_coords[:, 1])
        result.append((l1 * l2, l1, l2))
        # plt.plot(new_coords[:, 0], new_coords[:, 1]); plt.show()
    return result


def shaking_emulation_by_quat(image):
    from common.quaternion_utils import get_all_rotation_matrixs
    props = measure.regionprops(image)
    coords = props[0].coords
    rotations = 64
    # result_min = []
    # result_max = []
    result_axes_max = []
    result_axes_min = []
    for rotation_matrix in get_all_rotation_matrixs(rotations):
        new_coords = np.matmul(coords, rotation_matrix)
        l1 = np.max(new_coords[:, 0]) - np.min(new_coords[:, 0])
        l2 = np.max(new_coords[:, 1]) - np.min(new_coords[:, 1])
        l3 = np.max(new_coords[:, 2]) - np.min(new_coords[:, 2])
        #  (может быть достаточно найти только габариты по X  и Y).
        # будем рассматривать 3 оси!
        # result_min.append((min(l1, l2), l1, l2))
        # result_max.append((max(l1, l2), l1, l2))
        # result_axes.append(l1)
        # result_axes.append(l2)
        # result_axes.append(l3)
        result_axes_max.append(max(l1, l2))
        result_axes_max.append(max(l2, l3))
        result_axes_max.append(max(l1, l3))
        result_axes_min.append(min(l1, l2))
        result_axes_min.append(min(l2, l3))
        result_axes_min.append(min(l1, l3))
    return result_axes_max, result_axes_min


def calc_spatial_moment(image, x, y, z, i, j, k):
    return np.sum(x**i * y**j * z**k * image, dtype=np.float64)


def calc_central_moment(image, x, y, z, i, j, k, centroid: dict):
    return np.sum((x - centroid['mean_x'])**i * (y - centroid['mean_y'])**j * (z - centroid['mean_z'])**k * image,
                  dtype=np.float64)


def calc_normalized_moment(central_moments: dict, i, j, k, volume):
    return central_moments[f'{i}{j}{k}'] / volume**((i + j + k) / 2 + 1)


def get_moments(image) -> (dict, dict, dict, dict):
    spatial_moments = {}
    centroid = {}
    central_moments = {}
    normalized_moments = {}

    z, y, x = np.mgrid[:image.shape[0], :image.shape[1], :image.shape[2]]

    for i in range(0, 3):
        for j in range(0, 3):
            for k in range(0, 3):
                spatial_moments[f'{i}{j}{k}'] = calc_spatial_moment(image, x, y, z, i, j, k)

    centroid['mean_x'] = spatial_moments['100'] / spatial_moments['000']
    centroid['mean_y'] = spatial_moments['010'] / spatial_moments['000']
    centroid['mean_z'] = spatial_moments['001'] / spatial_moments['000']

    for i in range(0, 3):
        for j in range(0, 3):
            for k in range(0, 3):
                central_moments[f'{i}{j}{k}'] = calc_central_moment(image, x, y, z, i, j, k, centroid)

    # from skimage import measure
    # measure.moments_central(image)

    for i in range(0, 3):
        for j in range(0, 3):
            for k in range(0, 3):
                normalized_moments[f'{i}{j}{k}'] = calc_normalized_moment(central_moments, i, j, k,
                                                                          spatial_moments['000'])

    return centroid, spatial_moments, central_moments, normalized_moments


def calculate_zunic_compactness(central_moments: dict):
    return (3**(5 / 3) / 5 *
            np.pi**(2 / 3)) * (central_moments['000']**(5 / 3) /
                               (central_moments['002'] + central_moments['020'] + central_moments['200']))


def calc_inertia_tensor(central_moments):
    M_L = np.zeros((3, 3), dtype=np.float64)
    M_L[0, 0] = central_moments['020'] + central_moments['002']
    M_L[1, 1] = central_moments['200'] + central_moments['002']
    M_L[2, 2] = central_moments['200'] + central_moments['020']
    M_L[0, 1] = -central_moments['110']
    M_L[1, 0] = -central_moments['110']
    M_L[0, 2] = -central_moments['101']
    M_L[2, 0] = -central_moments['101']
    M_L[1, 2] = -central_moments['011']
    M_L[2, 1] = -central_moments['011']
    return M_L


def calculate_size_of_particle(coords, volume, inertia_tensor):
    w, eigenvec = LA.eig(inertia_tensor)

    L = np.zeros((3, 1), dtype=np.float64)
    L[0] = (10 * (w[1] + w[2] - w[0]) / volume)**(1 / 2)
    L[1] = (10 * (w[0] + w[2] - w[1]) / volume)**(1 / 2)
    L[2] = (10 * (w[0] + w[1] - w[2]) / volume)**(1 / 2)

    coords = np.matmul(coords, eigenvec)
    l1 = np.max(coords[:, 0]) - np.min(coords[:, 0])
    l2 = np.max(coords[:, 1]) - np.min(coords[:, 1])
    l3 = np.max(coords[:, 2]) - np.min(coords[:, 2])
    return (l1, l2, l3), L


def calc_equivalent_diameter(volume):
    return (6 * volume / np.pi)**(1 / 3)


def calc_feret(im3d):
    props = measure.regionprops(im3d)
    coords = props[0].coords
    from .diameter_feret import diameter3D, get_feret_by_quat, get_ferets
    estimate_max_ferets, estimate_min_ferets = diameter3D(coords, props[0].centroid)

    # min_feret, max_feret, mean_feret, median_feret, mean_among_projection, std_among_projection, gmean_among_projection = get_ferets(coords)
    min_feret, max_feret, mean_feret, median_feret, mean_among_projection, std_among_projection, gmean_among_projection,\
        f_min_feret, f_max_feret, f_mean_feret, f_median_feret, f_mean_among_projection, f_std_among_projection, f_gmean_among_projection= get_feret_by_quat(
        coords)
    return {
        'estimate_max_feret': estimate_max_ferets,
        'estimate_min_feret': estimate_min_ferets,
        'min_feret': min_feret,
        'max_feret': max_feret,
        'mean_feret': mean_feret,
        'median_feret': median_feret,
        'mean_among_projection': mean_among_projection,
        'std_among_projection': std_among_projection,
        'gmean_among_projection': gmean_among_projection,
        'extra': {
            'estimate_max_feret': estimate_max_ferets,
            'estimate_min_feret': estimate_min_ferets,
            'min_feret': f_min_feret,
            'max_feret': f_max_feret,
            'mean_feret': f_mean_feret,
            'median_feret': f_median_feret,
            'mean_among_projection': f_mean_among_projection,
            'std_among_projection': f_std_among_projection,
            'gmean_among_projection': f_gmean_among_projection
        }
    }


def calc_features(im3d):
    props = measure.regionprops(im3d)
    image = np.uint8(props[0].image)
    volume = props[0].area
    coords = props[0].coords

    centroid, spatial_moments, central_moments, normalized_moments = get_moments(image)
    equivalent_diameter = calc_equivalent_diameter(volume)  # props[0].equivalent_diameter
    zunic_compactness = calculate_zunic_compactness(central_moments)
    inertia_tensor = calc_inertia_tensor(central_moments)  # props[0].inertia_tensor * volume and change order
    (l1, l2, l3), L = calculate_size_of_particle(coords, volume, inertia_tensor)

    l_major = max(l1, l2, l3)
    l_minor = min(l1, l2, l3)

    print(f'l_major, l_minor: {l_major} : {l_minor}')
    print(f'semi axes: {L}')
    print(f'equivalent_diameter: {equivalent_diameter}')
    print(f'zunic_compactness: {zunic_compactness}')
    print(f'centriod: {centroid}')

    # result_axes = shaking_emulation(image)
    result_axes_max, result_axes_min = shaking_emulation_by_quat(image)
    min_min_axes = min(result_axes_min)
    min_max_axes = max(result_axes_min)
    max_min_axes = min(result_axes_max)
    max_max_axes = max(result_axes_max)
    # print(f'min, max axes by area: {min_axes, max_axes}')
    print(f'min_min_axes, min_max_axes, max_min_axes, max_max_axes axes by area:'
          f' {min_min_axes, min_max_axes, max_min_axes, max_max_axes}')

    return {
        'equivalent_diameter': equivalent_diameter,
        'by_tensor_major_axe': l_major,
        'by_tensor_minor_axe': l_minor,
        'by_tensor_l1_axe': l1,
        'by_tensor_l2_axe': l2,
        'by_tensor_l3_axe': l3,
        'by_tensor_semi_axes_a': L[0][0],
        'by_tensor_semi_axes_b': L[1][0],
        'by_tensor_semi_axes_c': L[2][0],
        'centroid_x': centroid['mean_x'],
        'centroid_y': centroid['mean_y'],
        'centroid_z': centroid['mean_z'],
        'by_shaking_min_axes_x': min_min_axes,
        'by_shaking_min_axes_y': max_min_axes,
        'by_shaking_max_axes_x': min_max_axes,
        'by_shaking_max_axes_y': max_max_axes,
        'zunic_compactness': zunic_compactness
    }
