import multiprocessing
import random

import numpy as np
from scipy import ndimage
from skimage import measure

from common.ellipsoid_utils import generate_border_ellipsoid, generate_ellipsoid
from common.geometry_utils import get_min_distance_between_point_and_object
from common.holes_utils import generate_holes


def get_sphere_radius(volume):
    return (volume * (3 / (4 * np.pi)))**(1 / 3)


def internal_hole_point(image, x, y, z):
    point_index = random.randint(0, len(z) - 1)
    return z[point_index], y[point_index], x[point_index]


def internal_hole_point_ver_2(image, x, y, z):
    point_index = random.randint(0, len(z))
    return z[point_index], y[point_index], x[point_index]


def get_internal_hole_radius(min_distance):
    # return min_distance
    # return random.randint(min_distance // 1.35, min_distance)
    return random.randint(1, min_distance - 2)


def get_internal_hole_radiusv2(low_bound, min_distance):
    # return min_distance
    # return random.randint(min_distance // 1.35, min_distance)
    return random.randint(low_bound, min_distance - 2)


def get_radius_triples(n, relation):
    from itertools import combinations
    result = []
    for i in range(1, n):
        for j in range(1, n):
            for k in range(1, n):
                possible_triple = (i, j, k)
                result_v = 1
                for elem_1, elem_2 in list(combinations(possible_triple, 2)):
                    if max(elem_1, elem_2) / min(elem_1, elem_2) <= relation:
                        result_v *= 1
                    else:
                        result_v *= 0
                if result_v == 1:
                    result.append(possible_triple)
    return result


def get_random_from_triples(triples):
    if len(triples) == 0:
        return None, None, None, False
    return max(triples), True
    result = random.choice(triples)
    # triples.remove(result)
    return result, True


def get_radiusesv2(maximal_possible_radius):
    triples = get_radius_triples(maximal_possible_radius, 1000)
    [r_x, r_y, r_z], status = get_random_from_triples(triples)
    return r_x, r_y, r_z, status


def get_holes_r(maximal_possible_radius):
    hole_r_x = get_internal_hole_radius(maximal_possible_radius)
    low_bound = hole_r_x // 3
    if low_bound == 0:
        low_bound = 1

    hole_r_y = get_internal_hole_radiusv2(low_bound, maximal_possible_radius)
    hole_r_z = get_internal_hole_radiusv2(low_bound, maximal_possible_radius)
    axes = [hole_r_x, hole_r_y, hole_r_z]
    random.shuffle(axes)
    return axes


def get_radiuses(maximal_possible_radius):
    # hole_r_x = get_internal_hole_radius(maximal_possible_radius)
    # hole_r_y = get_internal_hole_radius(maximal_possible_radius)
    # hole_r_z = get_internal_hole_radius(maximal_possible_radius)
    hole_r_x, hole_r_y, hole_r_z = get_holes_r(maximal_possible_radius)
    from itertools import combinations
    radiuses_pairs = list(combinations([hole_r_x, hole_r_y, hole_r_z], 2))
    should_stop = False
    result = 1
    for elem_1, elem_2 in radiuses_pairs:
        if max(elem_1, elem_2) / min(elem_1, elem_2) <= 3:
            result *= 1
        else:
            result *= 0
    if result == 1:
        should_stop = True
    return hole_r_x, hole_r_y, hole_r_z, should_stop


def get_holes_volume(orig_image, image):
    result = image - orig_image
    result[np.where(result != -1)] = 0
    result[np.where(result == -1)] = 1
    # props = measure.regionprops(result)
    return len(result[np.where(result == 1)])


def get_img_with_internal_hole(internal_ellipsoid,
                               orig_image,
                               image,
                               be_x_n,
                               be_y_n,
                               be_z_n,
                               x,
                               y,
                               z,
                               v_needed_holes=None,
                               wanted_r=None,
                               center=None):
    img_copy = image.copy()
    z_n, y_n, x_n = img_copy.nonzero()
    hole_center_z, hole_center_y, hole_center_x = internal_hole_point(internal_ellipsoid, x_n, y_n, z_n)

    if wanted_r is None:
        min_distance = get_min_distance_between_point_and_object(len(image), be_x_n, be_y_n, be_z_n, hole_center_x,
                                                                 hole_center_y, hole_center_z)

        max_radius = get_sphere_radius(v_needed_holes)
        if max_radius != 0:
            maximal_possible_radius = int(min_distance)
        else:
            maximal_possible_radius = int(min_distance)
        if maximal_possible_radius <= 2:
            return False, None, None, None
        # while True:
        #     hole_r_x, hole_r_y, hole_r_z, should_stop = get_radiuses(maximal_possible_radius)
        #     if should_stop:
        #         break
        hole_r_x, hole_r_y, hole_r_z, status_r = get_radiusesv2(maximal_possible_radius)
        print(status_r)
        if status_r is False:
            print(maximal_possible_radius, 'Status False')
            return False, None, None, None

        print(hole_r_x, hole_r_y, hole_r_z, maximal_possible_radius, multiprocessing.current_process().name)
    else:
        min_distance = get_min_distance_between_point_and_object(len(image), be_x_n, be_y_n, be_z_n, hole_center_x,
                                                                 hole_center_y, hole_center_z)
        if wanted_r >= min_distance:
            return False, None, None, None
        hole_r_x = wanted_r
        hole_r_y = wanted_r
        hole_r_z = wanted_r
        if center is not None:
            hole_center_y = center
            hole_center_x = center
            hole_center_z = center
    holes_img = generate_ellipsoid(x, y, z, hole_center_x, hole_center_y, hole_center_z, hole_r_x, hole_r_y, hole_r_z)
    # import scipy.io
    # scipy.io.savemat('test_img1.mat', dict(img=img_copy))
    # scipy.io.savemat('test_img2.mat', dict(img=holes_img))
    test_image, test_image_inner = generate_holes(img_copy, holes_img, '', internal_ellipsoid=internal_ellipsoid)
    # scipy.io.savemat('test_img_result.mat', dict(img=test_image))
    v_holes_current = get_holes_volume(orig_image, test_image)
    return True, test_image, v_holes_current, test_image_inner


def generate_internal_holes(image, ellipsoid_center, r_x, r_y, r_z, x, y, z, img_border, img_internal, porosity=0.1):
    k_max = len(image) // 4
    props = measure.regionprops(image)
    volume = props[0].area
    v_needed_holes = volume * porosity
    orig_image = image.copy()
    border_ellipsoid, internal_ellipsoid = img_border, img_internal
    be_z_n, be_y_n, be_x_n = border_ellipsoid.nonzero()
    k = 0
    attempts = 0
    v_holes_prev = 0
    upper_bound_limit = 700
    upper_bound_count = 0
    limit_attempts = 700
    percent_validate = 3
    while True:
        if attempts > limit_attempts or upper_bound_count > upper_bound_limit:
            if upper_bound_count > upper_bound_limit:
                print('NEEDTOTHINK')
            return False, None, None
        is_valid, test_image, v_holes_current, test_image_inner = get_img_with_internal_hole(
            internal_ellipsoid, orig_image, image, be_x_n, be_y_n, be_z_n, x, y, z, v_needed_holes)
        if not is_valid:
            upper_bound_count += 1
            continue
        if abs(1 - v_holes_current / v_needed_holes) * 100 < percent_validate:
            print(attempts, v_holes_current - v_holes_prev, v_holes_current, v_needed_holes)
            return True, test_image, v_holes_current / volume
        if v_holes_current > v_needed_holes:
            upper_bound_count += 1
            continue
        else:
            if v_holes_current - v_holes_prev > 0:
                # if ( v_holes_current - v_holes_prev) / v_holes_current * 100 > 10:
                print(v_holes_current - v_holes_prev, v_holes_current, v_needed_holes,
                      multiprocessing.current_process().name)
                v_holes_prev = v_holes_current
                k = k + 1
            else:
                # attempts += 1
                continue
            image = test_image
            internal_ellipsoid = test_image_inner
            if k <= k_max:
                continue
            r_last_hole = ((v_needed_holes - v_holes_current) * (3 / (4 * np.pi)))**(1 / 3)
            final_attempt = 0
            while True:
                is_valid, test_image, v_holes_current, test_image_inner = get_img_with_internal_hole(
                    internal_ellipsoid, orig_image, image, be_x_n, be_y_n, be_z_n, x, y, z, wanted_r=r_last_hole)
                if is_valid and abs(1 - v_holes_current / v_needed_holes) * 100 < percent_validate:
                    print(v_holes_current)
                    return True, test_image, v_holes_current / volume
                final_attempt += 1
                if final_attempt > limit_attempts:
                    print('FINAL LIMIT!')
                    return False, None, None
