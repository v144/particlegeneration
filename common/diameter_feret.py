import numpy as np
from numpy import cross, eye
from scipy.linalg import expm, norm
from scipy.spatial import ConvexHull
from scipy.stats.mstats import gmean


def get_feret_by_quat(coords):
    from common.quaternion_utils import get_all_rotation_matrixs
    rotations = 32
    result = []
    mean_l_results = []
    std_l_results = []
    gmean_l_results = []

    f_result = []
    f_mean_l_results = []
    f_std_l_results = []
    f_gmean_l_results = []

    for rotation_matrix in get_all_rotation_matrixs(rotations):
        new_coords = np.matmul(coords, rotation_matrix)
        # todo: maybe here we calc distance?
        l1 = np.max(new_coords[:, 0]) - np.min(new_coords[:, 0])
        l2 = np.max(new_coords[:, 1]) - np.min(new_coords[:, 1])
        l3 = np.max(new_coords[:, 2]) - np.min(new_coords[:, 2])

        indl1max = np.argmax(new_coords[:, 0])
        indl1min = np.argmin(new_coords[:, 0])
        indl2max = np.argmax(new_coords[:, 1])
        indl2min = np.argmin(new_coords[:, 1])
        indl3max = np.argmax(new_coords[:, 2])
        indl3min = np.argmin(new_coords[:, 2])

        f1 = np.linalg.norm(new_coords[indl1max] - new_coords[indl1min])
        f2 = np.linalg.norm(new_coords[indl2max] - new_coords[indl2min])
        f3 = np.linalg.norm(new_coords[indl3max] - new_coords[indl3min])
        f_result.append(f1)
        f_result.append(f2)
        f_result.append(f3)

        result.append(l1)
        result.append(l2)
        result.append(l3)
        mean_l_results.append(np.mean([l1, l2, l3]))
        std_l_results.append(np.sqrt(np.sum(np.square([l1, l2, l3]))))
        gmean_l_results.append(gmean([l1, l2, l3]))

        f_mean_l_results.append(np.mean([f1, f2, f3]))
        f_std_l_results.append(np.sqrt(np.sum(np.square([f1, f2, f3]))))
        f_gmean_l_results.append(gmean([f1, f2, f3]))

    return np.min(result), np.max(result), np.mean(result), np.median(result),\
           np.min(mean_l_results), np.min(std_l_results), np.min(gmean_l_results),\
           np.min(f_result), np.max(f_result), np.mean(f_result), np.median(f_result),\
           np.min(f_mean_l_results), np.min(f_std_l_results), np.min(f_gmean_l_results),




def get_rotation_matrix(axis, theta):
    return expm(cross(eye(3), axis / norm(axis) * theta))


def get_angle_between_3_points(centroid, p1, p2):
    centroid_p1 = p1 - centroid
    centroid_p2 = p2 - centroid
    cosine_angle = np.dot(centroid_p1, centroid_p2) / (np.linalg.norm(centroid_p1) * np.linalg.norm(centroid_p2))
    angle = np.arccos(cosine_angle)
    return np.degrees(angle)


def get_ferets(coords):
    import math
    axis = [4, 4, 1]
    result = []
    mean_l_results = []
    std_l_results = []
    gmean_l_results = []
    # plt.imshow(image.any(axis=2) * 255); plt.show()
    for i in np.arange(0.0, 180.0, 0.05):
        theta = math.radians(i)
        rotation_matrix = get_rotation_matrix(axis, theta)
        new_coords = np.matmul(coords, rotation_matrix)
        l1 = np.max(new_coords[:, 0]) - np.min(new_coords[:, 0])
        l2 = np.max(new_coords[:, 1]) - np.min(new_coords[:, 1])
        l3 = np.max(new_coords[:, 2]) - np.min(new_coords[:, 2])
        result.append(l1)
        result.append(l2)
        result.append(l3)
        mean_l_results.append(np.mean([l1, l2, l3]))
        std_l_results.append(np.sqrt(np.sum(np.square([l1, l2, l3]))))
        gmean_l_results.append(gmean([l1, l2, l3]))
    return min(result), max(result), sum(result) / len(result), np.median(result), min(mean_l_results), min(
        std_l_results), min(gmean_l_results)


def diameter3D(points, centroid):
    hull = ConvexHull(points)
    hull = points[hull.vertices]
    print(hull.shape)
    return compute3D(hull, centroid)


def compute3D(points, centroid):
    import numexpr as nexpr
    hull = ConvexHull(points)
    hull = points[hull.vertices]
    expr = "sum((a-p)**2, axis=1)"
    largest = [np.max(nexpr.evaluate(expr, local_dict={'a': hull[:i, :], 'p': hull[i]})) for i in range(1, len(hull))]
    # for estimate min feret diameter - min own result
    # second: think about 90 degrees
    # TODO: optimize it
    # own_result = []
    # for i in range(1, len(hull)):
    #     a = hull[:i, :]
    #     p = hull[i]
    #     angles = abs(get_angle_between_3_points(centroid, a, p))
    #     local_result = []
    #     for index, angle in enumerate(angles):
    #         if angle > 90:
    #             local_result.append((sum((a[index]-p)**2)))
    #     if local_result:
    #         own_result.append(max(local_result))
    # over by all - better estimation
    own_result = []
    for i in range(1, len(hull)):
        local_result = []
        for j in range(1, len(hull)):
            angle = abs(get_angle_between_3_points(centroid, hull[i], hull[j]))
            if angle > 90:
                local_result.append((sum((hull[i] - hull[j])**2)))
        if local_result:
            own_result.append(max(local_result))
    # np.sqrt(np.max(largest)) == np.sqrt(np.max(own_result))
    return np.sqrt(np.max(largest)), np.sqrt(np.min(own_result))
