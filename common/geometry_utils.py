def distance_between_two_points(p1x, p1y, p1z, p2x, p2y, p2z):
    return (((p1x - p2x)**2) + ((p1y - p2y)**2) + ((p1z - p2z)**2))**(1 / 2)


def get_min_distance_between_point_and_object(imsz, xp, yp, zp, center_x, center_y, center_z):
    # TODO: maybe optimize it!
    distance = imsz
    for i in range(0, len(xp)):
        distance = min(distance, distance_between_two_points(center_x, center_y, center_z, xp[i], yp[i], zp[i]))
    return distance


def min_distance_to_plane(x1, y1, z1, a, b, c, d):
    return abs((a * x1 + b * y1 + c * z1 + d)) / ((a * a + b * b + c * c)**(1 / 2))


def get_min_distance_to_border_image(imsz: int, center_x: int, center_y: int, center_z: int):
    distance = imsz
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 1, 0, 0, 0), distance)
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 1, 0, 0, -imsz), distance)
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 0, 1, 0, 0), distance)
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 0, 1, 0, -imsz), distance)
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 0, 0, 1, 0), distance)
    distance = min(min_distance_to_plane(center_x, center_y, center_z, 0, 0, 1, -imsz), distance)
    return distance
