import multiprocessing
import os

import numpy as np
import scipy.io as scio

from common.database_utils import (fetchall, get_db_connection, get_image_path, get_images, save_particle_common_meta,
                                   save_particle_feret_meta, save_particle_porosity, save_particle_shaking_axes,
                                   save_particle_tensor_meta)
from common.database_utils.feature_queries import GET_DISTRIBUTION_INDEX
from common.moments_utils import calc_features, calc_feret, calc_relation_closed_holes


def get_indexes():
    conn = get_db_connection()
    sizes = scio.loadmat('/Users/vano144/PycharmProjects/calculationAndGeneration3DInv/particle_file_store/dist.mat')
    with conn:
        database_indexes = fetchall(conn, GET_DISTRIBUTION_INDEX)

    result = []
    for database_index in database_indexes:
        result.append((sizes['X'][0][database_index[0]], database_index[0]))
    return result


def calc_features_for_image_group(image_group):
    conn = get_db_connection()

    with conn:
        for image_uuid, image_size in image_group:
            img_path = get_image_path(image_uuid, image_size)
            img = scio.loadmat(img_path)['img']
            features = calc_features(img)
            feret_features = calc_feret(img)
            save_particle_common_meta(conn, image_uuid, features)
            save_particle_shaking_axes(conn, image_uuid, features)
            save_particle_tensor_meta(conn, image_uuid, features)
            extra_f = feret_features.pop('extra')
            save_particle_feret_meta(conn, image_uuid, feret_features)
            save_particle_feret_meta(conn, image_uuid, extra_f, tb_name='particle_feret_meta_f')
            # now it is calculated in moment of generation
            # porosity_features = calc_relation_closed_holes(img)
            # save_particle_porosity(conn, image_uuid, porosity_features)
            # without holes section
            img_name = f'{image_uuid}_without_holes'
            img_path = get_image_path(img_name, image_size)
            img_without_holes = scio.loadmat(img_path)['img']
            features = calc_features(img_without_holes)
            save_particle_common_meta(conn, image_uuid, features, with_holes=False)
            save_particle_shaking_axes(conn, image_uuid, features, with_holes=False)
            save_particle_tensor_meta(conn, image_uuid, features, with_holes=False)


def calc_all_images_features_paralyzed():
    conn = get_db_connection()
    with conn:
        images = get_images(conn)
    cpus = os.cpu_count()
    image_groups = np.array_split(images, cpus)
    pool = multiprocessing.Pool()

    for image_group in image_groups:
        if len(image_group) == 0:
            continue
        # calc_features_for_image_group(image_group)
        pool.apply_async(calc_features_for_image_group, args=(image_group,))
    pool.close()
    pool.join()
