import random
from typing import Optional

import numpy as np
from numpy import ndarray
from scipy import ndimage

from settings import SHOW_STAGING_ROTATION

from .save_utlis import show_img


def generate_ellipsoid(x_grid: ndarray, y_grid: ndarray, z_grid: ndarray, xc: int, yc: int, zc: int, x_radius: int,
                       y_radius: int, z_radius: int):

    return np.int32(((x_grid - xc)**2) / (x_radius**2) + ((y_grid - yc)**2) / (y_radius**2) + ((z_grid - zc)**2) /
                    (z_radius**2) <= 1)


def generate_border_ellipsoid(x_grid: ndarray, y_grid: ndarray, z_grid: ndarray, xc: int, yc: int, zc: int,
                              x_radius: int, y_radius: int, z_radius: int):

    border_ellipsoid1 = np.int32((((x_grid - xc)**2) / (x_radius**2) + ((y_grid - yc)**2) / (y_radius**2) +
                                  ((z_grid - zc)**2) / (z_radius**2)) <= 1)
    x_radius *= 0.9
    y_radius *= 0.9
    z_radius *= 0.9
    border_ellipsoid2 = np.int32((((x_grid - xc)**2) / (x_radius**2) + ((y_grid - yc)**2) / (y_radius**2) +
                                  ((z_grid - zc)**2) / (z_radius**2)) <= 1)
    result = border_ellipsoid1 - border_ellipsoid2
    return result, border_ellipsoid2


def random_rotation(img: ndarray, comment: Optional[str] = None):
    for axes in ((0, 1), (1, 2), (0, 2)):
        random_angle_in_degrees = random.randint(0, 180)
        img = ndimage.rotate(img, axes=axes, angle=random_angle_in_degrees)
        if SHOW_STAGING_ROTATION:
            if comment:
                comment = f'{comment} \n --- AXES: {axes}; ANGLE: {random_angle_in_degrees}'
            show_img(img, comment)
    return img
