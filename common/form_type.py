import random
from typing import NoReturn


class FormType:
    # plate i/l > 0.6 s/i < 0.6 -> i > 0.6l and s < 0.6i
    # cube i/l > 0.6 s/i > 0.6 -> i > 0.6l and s > 0.6i
    # blade 0.6 > i/l > 0.35 s/i < 0.5 -> 0.35l < i < 0.6l and s < 0.5i
    # cuboid 0.6 > i/l > 0.35 s/i >  0.5 -> 0.35l < i < 0.6l and s > 0.5i
    # rod i/l < 0.35 -> i < 0.35l
    form_funcs = None

    def __init__(self):
        self.form_funcs = {
            'plate': self.generate_plate,
            'cube': self.generate_cube,
            'blade': self.generate_blade,
            'cuboid': self.generate_cuboid,
            'rod': self.generate_rod,
        }

    def get_sizes(self, imsz: int, form_type_name) -> [int, int, int]:
        sizes = list(self.form_funcs[form_type_name](imsz))
        random.shuffle(sizes)
        return sizes

    @staticmethod
    def generate_plate(imsz: int) -> (int, int, int):
        longest = random.randint(imsz // 3, imsz // 2.5)
        intermediate = random.randint(round(0.6 * longest), longest)
        shortest = random.randint(round(0.1 * intermediate), round(0.6 * intermediate))
        return longest, intermediate, shortest

    @staticmethod
    def generate_cube(imsz: int) -> (int, int, int):
        longest = random.randint(imsz // 3, imsz // 2.5)
        intermediate = random.randint(round(0.6 * longest), longest)
        shortest = random.randint(round(0.6 * intermediate), intermediate)
        return longest, intermediate, shortest

    @staticmethod
    def generate_blade(imsz: int) -> (int, int, int):
        longest = random.randint(imsz // 3, imsz // 2.5)
        intermediate = random.randint(round(0.35 * longest), round(0.6 * longest))
        shortest = random.randint(round(0.1 * intermediate), round(0.5 * intermediate))
        return longest, intermediate, shortest

    @staticmethod
    def generate_cuboid(imsz: int) -> (int, int, int):
        longest = random.randint(imsz // 3, imsz // 2.5)
        intermediate = random.randint(round(0.35 * longest), round(0.6 * longest))
        shortest = random.randint(round(0.5 * intermediate), intermediate)
        return longest, intermediate, shortest

    @staticmethod
    def generate_rod(imsz: int) -> (int, int, int):
        longest = random.randint(imsz // 3, imsz // 2.5)
        intermediate = random.randint(round(0.1 * longest), round(0.35 * longest))
        shortest = random.randint(round(0.1 * intermediate), intermediate)
        return longest, intermediate, shortest

    @classmethod
    def visualize_funcs(cls) -> NoReturn:
        l, i, s = cls.generate_plate(100)
        print("plate", f"i/l {i/l} s/i {s/i} {l, i, s}")

        l, i, s = cls.generate_cube(100)
        print("cube", f"i/l {i / l} s/i {s / i} {l, i, s}")

        l, i, s = cls.generate_blade(100)
        print("blade", f"i/l {i / l} s/i {s / i} {l, i, s}")

        l, i, s = cls.generate_cuboid(100)
        print("cuboid", f"i/l {i / l} s/i {s / i} {l, i, s}")

        l, i, s = cls.generate_rod(100)
        print("rob", f"i/l {i / l} s/i {s / i} {l, i, s}")


if __name__ == '__main__':
    form_type = FormType()
    print(form_type.get_sizes(100, 'plate'))
