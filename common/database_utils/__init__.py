import os
from uuid import uuid4

import psycopg2
import scipy.io as scio

from settings import PARTICLE_STORAGE, POSTGRES_ADDRESS


def get_image_path(img_uuid, img_size):
    return os.path.join(PARTICLE_STORAGE, str(img_size), f'{img_uuid}.mat')


def get_db_connection():
    return psycopg2.connect(POSTGRES_ADDRESS)


def save_without_porosity_particle(img_uuid, img_size, img):
    img_name = f'{img_uuid}_without_holes'
    img_path = get_image_path(img_name, img_size)
    os.makedirs(os.path.dirname(img_path), exist_ok=True)
    scio.savemat(img_path, dict(img=img))


def save_particle(connection, img_size, hole_amount, bulge_amount, distribution_index, img, entire_holes_amount):
    img_uuid = str(uuid4())
    img_path = get_image_path(img_uuid, img_size)
    os.makedirs(os.path.dirname(img_path), exist_ok=True)
    scio.savemat(img_path, dict(img=img))
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """INSERT INTO particle (id, image_size, hole_amount, bulge_amount, distribution_index, entire_holes_amount) VALUES (%s, %s, %s, %s, %s, %s)"""
        cursor.execute(sql_update_query,
                       (img_uuid, img_size, hole_amount, bulge_amount, distribution_index, entire_holes_amount))
        connection.commit()
        return img_uuid
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def get_images(connection):
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """select id, image_size from particle order by distribution_index;"""
        cursor.execute(sql_update_query)
        data = cursor.fetchall()
        return data
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def save_particle_shaking_axes(connection, img_uuid, features, with_holes=True):
    if not with_holes:
        img_uuid = f'{img_uuid}_with_holes'
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """INSERT INTO particle_shaking_axes (id, by_shaking_min_axes_x, by_shaking_min_axes_y, by_shaking_max_axes_x, by_shaking_max_axes_y, with_holes) VALUES (%s, %s, %s, %s, %s, %s)"""
        cursor.execute(sql_update_query,
                       (img_uuid, features['by_shaking_min_axes_x'], features['by_shaking_min_axes_y'],
                        features['by_shaking_max_axes_x'], features['by_shaking_max_axes_y'], with_holes))
        connection.commit()
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def save_particle_porosity(connection, img_uuid, features):
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """INSERT INTO particle_porosity_entity_holes (id, porosity) VALUES (%s, %s)"""
        cursor.execute(sql_update_query, (img_uuid, features['porosity']))
        connection.commit()
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def save_particle_tensor_meta(connection, img_uuid, features, with_holes=True):
    if not with_holes:
        img_uuid = f'{img_uuid}_with_holes'
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """INSERT INTO particle_tensor_meta (id, by_tensor_major_axe, by_tensor_minor_axe, by_tensor_semi_axes_a, by_tensor_semi_axes_b, by_tensor_semi_axes_c, by_tensor_l1_axe, by_tensor_l2_axe, by_tensor_l3_axe, with_holes) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        cursor.execute(
            sql_update_query,
            (img_uuid, features['by_tensor_major_axe'], features['by_tensor_minor_axe'],
             features['by_tensor_semi_axes_a'], features['by_tensor_semi_axes_b'], features['by_tensor_semi_axes_c'],
             features['by_tensor_l1_axe'], features['by_tensor_l2_axe'], features['by_tensor_l3_axe'], with_holes))
        connection.commit()
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def save_particle_common_meta(connection, img_uuid, features, with_holes=True):
    if not with_holes:
        img_uuid = f'{img_uuid}_with_holes'
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = """INSERT INTO particle_common_meta (id, equivalent_diameter, centroid_x, centroid_y, centroid_z, zunic_compactness, with_holes) VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        cursor.execute(sql_update_query,
                       (img_uuid, features['equivalent_diameter'], features['centroid_x'], features['centroid_y'],
                        features['centroid_z'], features['zunic_compactness'], with_holes))
        connection.commit()
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def save_particle_feret_meta(connection, img_uuid, features, tb_name='particle_feret_meta'):
    cursor = None
    try:
        cursor = connection.cursor()
        sql_update_query = f"""INSERT INTO {tb_name} (id, estimate_max_feret, min_feret, max_feret, mean_feret, median_feret, std_among_projection, mean_among_projection, gmean_among_projection, estimate_min_feret) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        cursor.execute(
            sql_update_query,
            (img_uuid, features['estimate_max_feret'], features['min_feret'], features['max_feret'],
             features['mean_feret'], features['median_feret'], features['std_among_projection'],
             features['mean_among_projection'], features['gmean_among_projection'], features['estimate_min_feret']))
        connection.commit()
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def fetchall(connection, q):
    cursor = None
    try:
        cursor = connection.cursor()
        cursor.execute(q)
        data = cursor.fetchall()
        return data
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")


def fetchall_with_kwargs(connection, q, *args):
    cursor = None
    try:
        cursor = connection.cursor()
        cursor.execute(q, args)
        data = cursor.fetchall()
        return data
    except Exception as error:
        print("Error in update operation", error)
    finally:
        if cursor:
            cursor.close()
            print("PostgreSQL cursor is closed")
