# shaking queries - standard
GET_MAX_SHAKING = 'select GREATEST(by_shaking_min_axes_x, by_shaking_min_axes_y) from particle_shaking_axes where with_holes=%s'
GET_MIN_SHAKING = 'select LEAST(by_shaking_min_axes_x, by_shaking_min_axes_y) from particle_shaking_axes where with_holes=%s'
GET_VALID_SHAKING = 'select by_shaking_min_axes_y from particle_shaking_axes where with_holes=%s'

# by tensor queries
GET_MAX_TENSOR_AXE = 'select by_tensor_major_axe from particle_tensor_meta where with_holes=%s'
GET_MIN_TENSOR_AXE = 'select by_tensor_minor_axe from particle_tensor_meta where with_holes=%s'
GET_MEAN_TENSOR_AXE = 'select (by_tensor_major_axe + by_tensor_minor_axe) / 2 from particle_tensor_meta where with_holes=%s'
GET_TENSOR_AXES = 'select by_tensor_l1_axe, by_tensor_l2_axe, by_tensor_l3_axe from particle_tensor_meta where with_holes=%s'

# axes queries
GET_MIN_SEMI_AXE = 'select LEAST(by_tensor_semi_axes_a, by_tensor_semi_axes_b,by_tensor_semi_axes_c) from particle_tensor_meta where with_holes=%s'
GET_MAX_SEMI_AXE = 'select GREATEST(by_tensor_semi_axes_a, by_tensor_semi_axes_b,by_tensor_semi_axes_c) from particle_tensor_meta where with_holes=%s'
GET_MEAN_SEMI_AXE = 'select (by_tensor_semi_axes_a + by_tensor_semi_axes_b + by_tensor_semi_axes_c) / 3 from particle_tensor_meta where with_holes=%s'
GET_SEMI_AXES = 'select by_tensor_semi_axes_a, by_tensor_semi_axes_b, by_tensor_semi_axes_c from particle_tensor_meta where with_holes=%s'

# equivalent_diameter queries
GET_ED = 'select equivalent_diameter from particle_common_meta where with_holes=%s'

# feret queries
# GET_MIN_FERET = 'select min_feret from particle_feret_meta_f'
# GET_MAX_FERET = 'select max_feret from particle_feret_meta_f'
# GET_MEAN_FERET = 'select (min_feret + max_feret) / 2 from particle_feret_meta_f'
# GET_GMEAN_AP_FERET = 'select gmean_among_projection from particle_feret_meta_f'
# GET_MEAN_AP_FERET = 'select mean_among_projection from particle_feret_meta_f'
# GET_STD_AP_FERET = 'select std_among_projection from particle_feret_meta_f'
# GET_MEDIAN_FERET = 'select median_feret from particle_feret_meta_f'

# feret queries
GET_MIN_FERET = 'select min_feret from particle_feret_meta'
GET_MAX_FERET = 'select max_feret from particle_feret_meta'
GET_MEAN_FERET = 'select (min_feret + max_feret) / 2 from particle_feret_meta'
GET_GMEAN_AP_FERET = 'select gmean_among_projection from particle_feret_meta'
GET_MEAN_AP_FERET = 'select mean_among_projection from particle_feret_meta'
GET_STD_AP_FERET = 'select std_among_projection from particle_feret_meta'
GET_MEDIAN_FERET = 'select median_feret from particle_feret_meta'

# estimation feret
GET_MEAN_FERET_ESTIMATION = 'select (estimate_min_feret + estimate_max_feret) / 2 from particle_feret_meta_f'
GET_MIN_FERET_ESTIMATION = 'select estimate_min_feret from particle_feret_meta_f'
GET_MAX_FERET_ESTIMATION = 'select estimate_max_feret from particle_feret_meta_f'

GET_MEAN_AXES_TENSOR_UPDATED_QUERY = '''
WITH gg AS (
	SELECT array_agg(foo.tensor) as tensors, foo.id
	FROM (
		SELECT by_tensor_semi_axes_a as tensor, id
		from particle_tensor_meta
		UNION
		SELECT by_tensor_semi_axes_b as tensor, id
		from particle_tensor_meta
		UNION
		SELECT by_tensor_semi_axes_c as tensor, id
		from particle_tensor_meta
	) as foo
	GROUP BY id
)
SELECT 
	tensors[2] as median,
	tensors[2:3] as two_min,
	id
FROM gg

'''

GET_DISTRIBUTION_INDEX = 'SELECT a.n from generate_series(0, 999) as a(n) left join particle on a.n = particle.distribution_index where particle.distribution_index is null'
