from typing import Optional

import matplotlib.pyplot as plt
import scipy.io
from mpl_toolkits.mplot3d import Axes3D  # noqa
from numpy import ndarray


def save_to_mat(img: ndarray, filename: str = 'MY3D.mat'):
    z1, y1, x1 = img.nonzero()
    scipy.io.savemat(filename, dict(x=x1, y=y1, z=z1))


def show_img(img: ndarray, comment: Optional[str] = None):

    z1, y1, x1 = img.nonzero()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x1, y1, z1, zdir='z', c='red')
    if comment is not None:
        ax.set_title(f'title: {comment}')
    plt.show()


def show_mayavi_img(img_path: str, img_path_without_holes: str):
    from mayavi import mlab
    import scipy.io as scio
    img = scio.loadmat(img_path)['img']
    img_without_holes = scio.loadmat(img_path_without_holes)['img']
    holes_img = img_without_holes - img
    x_h, y_h, z_h = holes_img.nonzero()
    mlab.contour3d(img_without_holes, color=(0, 1, 0), opacity=0.05, transparent=True)
    mlab.contour3d(holes_img, color=(1, 0, 0), opacity=0.05, transparent=True)
    # mlab.points3d(x_h, y_h, z_h, color=(1, 0, 0), mode='point', scale_factor=1)
    mlab.show()


if __name__ == '__main__':
    ff = 'd6bee16e-2288-4541-ad6b-81517229da60'
    i1 = f'/Users/vano144/PycharmProjects/calculationAndGeneration3DInv/particle_file_store/134/{ff}.mat'
    i2 = f'/Users/vano144/PycharmProjects/calculationAndGeneration3DInv/particle_file_store/134/{ff}_without_holes.mat'
    # i1 = "/Users/vano144/PycharmProjects/calculationAndGeneration3DInv/common/41c70ee1-d115-49f1-b91c-e78c32789dec.mat"
    # i2 = "/Users/vano144/PycharmProjects/calculationAndGeneration3DInv/common/41c70ee1-d115-49f1-b91c-e78c32789dec_without_holes.mat"
    show_mayavi_img(i1,i2)
