CREATE TABLE particle (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    image_size SMALLINT,
    hole_amount SMALLINT,
    bulge_amount SMALLINT,
    distribution_index SMALLINT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);