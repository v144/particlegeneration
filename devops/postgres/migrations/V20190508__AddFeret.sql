CREATE TABLE particle_feret_meta (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    estimate_max_feret FLOAT,
    min_feret FLOAT,
    max_feret FLOAT,
    mean_feret FLOAT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);