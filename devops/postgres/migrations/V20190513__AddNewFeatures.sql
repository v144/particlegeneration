ALTER TABLE particle_feret_meta ADD COLUMN median_feret FLOAT,
 ADD COLUMN std_among_projection FLOAT,
 ADD COLUMN mean_among_projection FLOAT,
 ADD COLUMN gmean_among_projection FLOAT;

ALTER TABLE particle_tensor_meta ADD COLUMN by_tensor_l1_axe FLOAT,
 ADD COLUMN by_tensor_l2_axe FLOAT,
 ADD COLUMN by_tensor_l3_axe FLOAT;