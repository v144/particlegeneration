CREATE TABLE particle_shaking_axes (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    by_shaking_min_axes_x FLOAT,
    by_shaking_min_axes_y FLOAT,
    by_shaking_max_axes_x FLOAT,
    by_shaking_max_axes_y FLOAT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE particle_tensor_meta (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    by_tensor_major_axe FLOAT,
    by_tensor_minor_axe FLOAT,
    by_tensor_semi_axes_a FLOAT,
    by_tensor_semi_axes_b FLOAT,
    by_tensor_semi_axes_c FLOAT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE particle_common_meta (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    equivalent_diameter FLOAT,
    centroid_x FLOAT,
    centroid_y FLOAT,
    centroid_z FLOAT,
    zunic_compactness FLOAT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);