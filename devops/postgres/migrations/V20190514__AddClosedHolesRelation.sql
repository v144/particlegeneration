CREATE TABLE particle_porosity_entity_holes (
    id character varying(36) PRIMARY KEY NOT NULL DEFAULT uuid_in((md5((random())::text))::cstring),
    porosity FLOAT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);