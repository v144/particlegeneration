ALTER TABLE particle_feret_meta_f ADD COLUMN median_feret FLOAT,
 ADD COLUMN std_among_projection FLOAT,
 ADD COLUMN mean_among_projection FLOAT,
 ADD COLUMN gmean_among_projection FLOAT;