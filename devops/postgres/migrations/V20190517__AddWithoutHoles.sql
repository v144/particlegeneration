ALTER TABLE particle_shaking_axes ALTER COLUMN id TYPE varchar(100);
ALTER TABLE particle_tensor_meta ALTER COLUMN id TYPE varchar(100);
ALTER TABLE particle_common_meta ALTER COLUMN id TYPE varchar(100);


ALTER TABLE particle_shaking_axes ADD COLUMN with_holes BOOLEAN DEFAULT TRUE;
ALTER TABLE particle_tensor_meta ADD COLUMN with_holes BOOLEAN DEFAULT TRUE;
ALTER TABLE particle_common_meta ADD COLUMN with_holes BOOLEAN DEFAULT TRUE;