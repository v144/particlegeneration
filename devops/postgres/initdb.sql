CREATE ROLE particles
LOGIN
PASSWORD 'postgres'
NOSUPERUSER
NOINHERIT
NOCREATEDB
NOCREATEROLE
NOREPLICATION;

CREATE DATABASE particles
    WITH OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

ALTER ROLE particles IN DATABASE particles SET search_path = particles;


\c particles
CREATE SCHEMA particles;

ALTER SCHEMA particles
    OWNER TO particles;